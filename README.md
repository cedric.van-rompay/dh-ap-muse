# Setup

- Install `libsodium` by following instructions at https://download.libsodium.org/doc/installation/
- Install `libbloom` (https://github.com/jvirkki/libbloom)
- Update linker database by running `sudo ldconfig`

# Build

`make`

# Run

`make check`

# from https://download.libsodium.org/doc/usage/
# translated from shell syntax to make syntax
# (see https://stackoverflow.com/a/28533513/3025740)
CFLAGS=`pkg-config --cflags libsodium`
LDFLAGS=`pkg-config --libs libsodium`

.PHONY: default
default: dirs bin/dh-ap-muse

bin/dh-ap-muse: dh-ap-muse.c
	gcc -o $@ $^ -lbloom $(CFLAGS) $(LDFLAGS)

.PHONY: dirs
dirs:
	mkdir -p bin

.PHONY: check
check: bin/dh-ap-muse
	bin/dh-ap-muse

#include <string.h>
#include <stdio.h>
#include <time.h>

#include <sodium.h>
#include "bloom.h"

# define NUMBER_OF_KEYWORDS 1000
# define KEYWORD_LENGTH 16


// hash a keyword into an element of the DH-hard group
// the "trick" is to hash into a DH secret key
// and derive a DH public key from it
int hash_keyword(unsigned char* keyword_hash, const unsigned char *keyword){
    unsigned char exponent_hash[crypto_scalarmult_SCALARBYTES];

    int keyword_length = strlen(keyword);

    crypto_generichash(exponent_hash, sizeof exponent_hash, keyword, keyword_length, NULL, 0);    
    crypto_scalarmult_base(keyword_hash, exponent_hash);

    return 0;
}

int encrypt_keyword(unsigned char *encrypted_kw, const unsigned char *key, const unsigned char *keyword){
    unsigned char keyword_hash[crypto_scalarmult_BYTES];

    hash_keyword(keyword_hash, keyword);

    if(crypto_scalarmult(encrypted_kw, key, keyword_hash) != 0){
        printf("ERROR during application of key (crypto_scalarmult)");
        return 1;
    }

    return 0;
}

int transform_encrypted_keyword(unsigned char *transformed_kw, const unsigned char *key, const unsigned char *encrypted_kw){
    if(crypto_scalarmult(transformed_kw, key, encrypted_kw) != 0){
        printf("ERROR during application of key (crypto_scalarmult)");
        return 1;
    }

    return 0;
}

int main(void) {
    if (sodium_init() < 0) {
        /* panic! the library couldn't be initialized, it is not safe to use */
        return 1;
    }
    
    // We are assuming crypto_generichash_BYTES == crypto_scalarmult_SCALARBYTES
    if (crypto_generichash_BYTES != crypto_scalarmult_SCALARBYTES){
        printf("ERROR crypto_generichash_BYTES != crypto_scalarmult_SCALARBYTES");
        return 1;
    }

    // randomly generated keywords
    unsigned char keywords[NUMBER_OF_KEYWORDS][KEYWORD_LENGTH];
    for(int i = 0; i < NUMBER_OF_KEYWORDS; i++){
        randombytes_buf(keywords[i], KEYWORD_LENGTH);
    }


    // from https://download.libsodium.org/doc/advanced/scalar_multiplication.html
    unsigned char record_key[crypto_box_SECRETKEYBYTES];
    randombytes_buf(record_key, sizeof record_key);

    unsigned char blinding_factor[crypto_box_SECRETKEYBYTES];
    randombytes_buf(blinding_factor, sizeof blinding_factor);

    // testing correctness

    // writer side
    unsigned char encrypted_kw[crypto_scalarmult_BYTES];
    encrypt_keyword(encrypted_kw, record_key, keywords[0]);

    unsigned char prepared_kw[crypto_scalarmult_BYTES];
    transform_encrypted_keyword(prepared_kw, blinding_factor, encrypted_kw);

    // reader side
    unsigned char trapdoor[crypto_scalarmult_BYTES];
    encrypt_keyword(trapdoor, blinding_factor, keywords[0]);

    unsigned char transformed_trapdoor[crypto_scalarmult_BYTES];
    transform_encrypted_keyword(transformed_trapdoor, record_key, trapdoor);

    // should be equal
    if(memcmp(transformed_trapdoor, prepared_kw, sizeof(prepared_kw))){
        printf("ERROR transformed trapdoor and prepared kw not equal (memcmp)");
        return 1;
    }

    // benchmarking
    
    printf("record size: %i\n", NUMBER_OF_KEYWORDS);
    
    float starttime, endtime;

    unsigned char encrypted_keywords[NUMBER_OF_KEYWORDS][crypto_scalarmult_BYTES];

    starttime = (float)clock()/CLOCKS_PER_SEC;
    for(int i = 0; i < NUMBER_OF_KEYWORDS; i++){
        encrypt_keyword(encrypted_keywords[i], record_key, keywords[i]);
    }
    endtime = (float)clock()/CLOCKS_PER_SEC;

    float encryption_time = (endtime - starttime);
    printf("encryption time: %f\n", encryption_time);

    // transformation
    // (transforming an encrypted keyword is the same as transforming a trapdoor)

    unsigned char transformed_keywords[NUMBER_OF_KEYWORDS][crypto_scalarmult_BYTES];
    starttime = (float)clock()/CLOCKS_PER_SEC;
    for(int i = 0; i < NUMBER_OF_KEYWORDS; i++){
        transform_encrypted_keyword(transformed_keywords[i], blinding_factor, encrypted_keywords[i]);
    }
    endtime = (float)clock()/CLOCKS_PER_SEC;

    float transformation_time = (endtime - starttime);
    printf("transformation time: %f\n", transformation_time);

    // record preparation
    // (that is, transformation and insertion in a hash structure)

    starttime = (float)clock()/CLOCKS_PER_SEC;

    struct bloom bloom;
    bloom_init(&bloom, NUMBER_OF_KEYWORDS, 0.01);
    unsigned char buffer[crypto_scalarmult_BYTES];

    for(int i = 0; i < NUMBER_OF_KEYWORDS; i++){
        transform_encrypted_keyword(buffer, blinding_factor, encrypted_keywords[i]);
        bloom_add(&bloom, buffer, crypto_scalarmult_BYTES);
    }

    endtime = (float)clock()/CLOCKS_PER_SEC;

    float preparation_time = (endtime - starttime);
    printf("preparation speed: %f\n", transformation_time);


    if (!bloom_check(&bloom, transformed_keywords[1], crypto_scalarmult_BYTES)) {
        printf("ERROR keyword not found\n");
    }

    return 0;
}

